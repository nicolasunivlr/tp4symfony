TP4
---

Pour récupérer le projet

```sh
cd /media/Qi/$USER
git clone https://framagit.org/nicolasunivlr/tp4symfony.git
cd tp4symfony
composer install
```

- on démarre mysql
- on crée le fichier .env.local

```sh
bin/console server:run
```